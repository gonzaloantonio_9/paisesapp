package com.viewscomplex.appjc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.appjc.databinding.ActivityCatalogoBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.model.appjc.Paises;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogoActivity extends AppCompatActivity {
    private ActivityCatalogoBinding binding;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCatalogoBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.recyclerViewProductos.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerViewProductos.setLayoutManager(layoutManager);
        db = FirebaseFirestore.getInstance();

        // List<Paises> paises = getListaProductos();
        //insertarFireBase(paises);
        List<Paises> paises = consultarFireBase();
        RecyclerView.Adapter mAdapter = new AdaptadorPaises(paises);
        binding.recyclerViewProductos.setAdapter(mAdapter);
    }

    protected List<Paises> getListaProductos() {
        List<Paises> paises = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open("paises.csv")));
            String line;
            Log.e("Reader Stuff", reader.readLine());
            while ((line = reader.readLine()) != null) {
                Log.e("code", line);
                String[] d = line.split(";");

                Paises p = new Paises(Integer.parseInt(d[0]), d[1], d[2], d[3]);
                paises.add(p);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return paises;
    }

    public void insertarFireBase(List<Paises> paises) {
        for (Paises pais : paises) {
            Map<String, Object> mapa = getMapa(pais);
            db.collection("pais")
                    .add(mapa)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("FB", "Producto amazon agregado correctamente:" + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("FB", "Error producto amazon no agregado", e);
                        }
                    });
        }
    }

    private Map<String, Object> getMapa(Paises paises) {
        Map<String, Object> mapa = new HashMap<>();
        mapa.put("codigo", paises.getCodigo());
        mapa.put("nombre", paises.getNombre());
        mapa.put("fotoURL", paises.getFotoURL());
        mapa.put("videoURL", paises.getVideoURL());
        return mapa;
    }

    public List<Paises> consultarFireBase() {
        final List<Paises> paises = new ArrayList<>();
        db.collection("pais")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                //Log.d("FB", document.get("fotoURL").toString());
                                Paises pais = new Paises();
                                pais.setCodigo(Integer.valueOf(document.get("codigo").toString()));
                                pais.setNombre(document.get("nombre").toString());
                                pais.setFotoURL(document.get("fotoURL").toString());
                                pais.setVideoURL(document.get("videoURL").toString());
                                paises.add(pais);
                            }
                        }
                    }
                });
        return paises;
    }
}