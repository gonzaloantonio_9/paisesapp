package com.intents.appjc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.appjc.R;

public class SecundariaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secundaria);
        String mensaje = "Bienvenido: " +  getIntent().getStringExtra(Intent.EXTRA_TEXT);
        Toast.makeText(this, mensaje,Toast.LENGTH_LONG).show();
    }
}