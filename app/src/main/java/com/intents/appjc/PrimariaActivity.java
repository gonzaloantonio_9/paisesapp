package com.intents.appjc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.appjc.databinding.ActivityPrimariaBinding;

public class PrimariaActivity extends AppCompatActivity {

    private ActivityPrimariaBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPrimariaBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }

    public void onClickIrActividad(View view) {
        Intent intent = new Intent(this,SecundariaActivity.class);
        String email = binding.editTextTextEmailAddress.getText().toString();
        intent.putExtra(Intent.EXTRA_TEXT,email);
        startActivity(intent);
    }
}