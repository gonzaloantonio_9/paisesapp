# Country Aplications
### Información a mostrar

- Nombre del País
- Bandera
- Himno Nacional

### Vistas
![Imagenes de Banderas 1](https://paisesapp.000webhostapp.com/img/banderasUno.jpeg)

![Imagenes de Banderas 2](https://paisesapp.000webhostapp.com/img/banderasDos.jpeg)

![Imagenes de Banderas 3](https://paisesapp.000webhostapp.com/img/banderasTres.jpeg)

![Imagenes de Banderas 4](https://paisesapp.000webhostapp.com/img/banderasCuatro.jpeg)

![Imagenes de Banderas 5](https://paisesapp.000webhostapp.com/img/banderasCinco.jpeg)

![Imagenes de Banderas 6](https://paisesapp.000webhostapp.com/img/banderasSeis.jpeg)

### Dependecias

- Glide
> 	dependencies{
		implementation 'com.github.bumptech.glide: glide:4.11.0'
		annotationProcessor 'com.github.bumptech.glide: compiler:4.11.0'
		}

- Youtube Player
> 	dependencies{
		implementation 'com.pierfrancescosoffritti.androidyoutubeplayer: core:10.0.5'
		}

- RecyclerView
> 	dependencies{
		implementation 'androidx.recyclerview: recyclerview:1.1.0'
		}

- CardView
> 	dependencies{
		implementation 'androidx.cardview: cardview:1.0.0'
		}